﻿using System;
using System.Collections.Generic;

namespace LynxDbApi.Models
{
    public partial class ScooterPart
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public decimal? Price { get; set; }
        public int VendorId { get; set; }
        public bool IsDeleted { get; set; }
    }
}
