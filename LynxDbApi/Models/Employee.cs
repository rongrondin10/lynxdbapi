﻿using System;
using System.Collections.Generic;

namespace LynxDbApi.Models
{
    public partial class Employee
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int? RoleId { get; set; }
        public int? TerritoryId { get; set; }
        public string FullName { get; set; }
        public string Password { get; set; }
        public string RoleType { get; set; }
        public string TerritoryName { get; set; }
        public bool IsDeleted { get; set; }
    }
}
