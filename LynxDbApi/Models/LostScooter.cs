﻿using System;
using System.Collections.Generic;

namespace LynxDbApi.Models
{
    public partial class LostScooter
    {
        public int Id { get; set; }
        public int ScooterId { get; set; }
        public int TechnicianId { get; set; }
        public string ModelNumber { get; set; }
        public DateTime DateAddedUtc { get; set; }
        public int StatusId { get; set; }
        public string StatusType { get; set; }
        public string Notes { get; set; }
        public bool IsDeleted { get; set; }
    }
}
