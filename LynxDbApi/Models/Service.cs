﻿using System;
using System.Collections.Generic;

namespace LynxDbApi.Models
{
    public partial class Service
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public int TechnicianId { get; set; }
        public int ScooterId { get; set; }
        public int? TerritoryId { get; set; }
        public DateTime DateServicedUtc { get; set; }
        public int StatusId { get; set; }
        public string Notes { get; set; }
        public string ScooterModel { get; set; }
        public string TerritoryName { get; set; }
        public string StatusType { get; set; }
        public string EmployeeName { get; set; }
        public string TechName { get; set; }
        public bool? Repaired { get; set; }
        public bool? Found { get; set; }
        public bool? Retrieved { get; set; }
        public bool? Pending { get; set; }
        public string PendingNotes { get; set; }
        public int? RepairTechId { get; set; }
        public DateTime? RepairDate { get; set; }
        public int? FoundTechId { get; set; }
        public DateTime? FoundDate { get; set; }
        public int? RetrieveTechId { get; set; }
        public DateTime? RetrieveDate { get; set; }
        public string RepairNotes { get; set; }
        public DateTime? PendingDate { get; set; }
        public int? RepairTaskId { get; set; }
        public string RepairTask { get; set; }
        public int? RepairTime { get; set; }
        public bool IsDeleted { get; set; }
    }
}
