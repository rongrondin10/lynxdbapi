﻿using System;
using System.Collections.Generic;

namespace LynxDbApi.Models
{
    public partial class Vendor
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Notes { get; set; }
        public bool IsDeleted { get; set; }
    }
}
