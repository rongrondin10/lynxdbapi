﻿using System;
using System.Collections.Generic;

namespace LynxDbApi.Models
{
    public partial class Status
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public bool IsDeleted { get; set; }
    }
}
