﻿using System;
using System.Collections.Generic;

namespace LynxDbApi.Models
{
    public partial class KeepRental
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zipcode { get; set; }
        public string Email { get; set; }
        public byte[] DriversLicense { get; set; }
        public string RentalAgreement { get; set; }
        public bool? Insurance { get; set; }
        public double? RentalPrice { get; set; }
        public bool? Stripe { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? DateDelivered { get; set; }
        public int? DeliveredBy { get; set; }
        public bool? ChargerDelivered { get; set; }
        public DateTime? RequestEndDate { get; set; }
        public DateTime? RentalEndDate { get; set; }
        public bool? ChargerCollected { get; set; }
        public string Notes { get; set; }
        public bool IsDeleted { get; set; }
    }
}
