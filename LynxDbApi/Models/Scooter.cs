﻿using System;
using System.Collections.Generic;

namespace LynxDbApi.Models
{
    public partial class Scooter
    {
        public int Id { get; set; }
        public string ModelNumber { get; set; }
        public int TerritoryId { get; set; }
        public DateTime? DateAdded { get; set; }
        public string TerritoryName { get; set; }
        public bool? Damaged { get; set; }
        public bool? Missing { get; set; }
        public bool? Irretrievable { get; set; }
        public bool? Lost { get; set; }
        public bool? Stolen { get; set; }
        public bool? Totaled { get; set; }
        public string LostNotes { get; set; }
        public string StolenNotes { get; set; }
        public string TotaledNotes { get; set; }
        public string Imei { get; set; }
        public bool IsDeleted { get; set; }
    }
}
