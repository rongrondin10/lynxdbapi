﻿using System;
using System.Collections.Generic;

namespace LynxDbApi.Models
{
    public partial class RepairTask
    {
        public int Id { get; set; }
        public string Task { get; set; }
        public int? RepairTime { get; set; }
        public bool IsDeleted { get; set; }
    }
}
