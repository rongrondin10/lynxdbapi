﻿using System;
using System.Collections.Generic;

namespace LynxDbApi.Models
{
    public partial class Territory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public bool IsDeleted { get; set; }
    }
}
