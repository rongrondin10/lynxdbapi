﻿using System;
using System.Collections.Generic;

namespace LynxDbApi.Models
{
    public partial class RepairStatus
    {
        public int Id { get; set; }
        public string Status { get; set; }
        public bool IsDeleted { get; set; }
    }
}
