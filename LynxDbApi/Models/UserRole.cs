﻿using System;
using System.Collections.Generic;

namespace LynxDbApi.Models
{
    public partial class UserRole
    {
        public int Id { get; set; }
        public string Role { get; set; }
        public bool IsDeleted { get; set; }
    }
}
