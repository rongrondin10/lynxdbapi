﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace LynxDbApi.Models
{
    public partial class UserFeedback
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Username { get; set; }
        public string FeedbackType { get; set; }
        public string Feedback { get; set; }
        public DateTime DateTime { get; set; }
    }
}
