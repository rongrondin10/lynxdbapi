﻿using System;
using System.Collections.Generic;

namespace LynxDbApi.Models
{
    public partial class TotaledScooter
    {
        public int Id { get; set; }
        public int ScooterId { get; set; }
        public int TechnicianId { get; set; }
        public string ModelNumber { get; set; }
        public DateTime DateAddedUtc { get; set; }
        public string Notes { get; set; }
        public bool IsDeleted { get; set; }
    }
}
