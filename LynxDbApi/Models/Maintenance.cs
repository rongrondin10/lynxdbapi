﻿using System;
using System.Collections.Generic;

namespace LynxDbApi.Models
{
    public partial class Maintenance
    {
        public int Id { get; set; }
        public int ServiceId { get; set; }
        public int TechnicianId { get; set; }
        public string ModelNumber { get; set; }
        public int RepairStatus { get; set; }
        public string Notes { get; set; }
        public decimal? RepairCost { get; set; }
        public string PartsList { get; set; }
        public DateTime DateAddedUtc { get; set; }
        public string TechName { get; set; }
        public int ScooterId { get; set; }
        public bool? Pending { get; set; }
        public bool IsDeleted { get; set; }
    }
}
