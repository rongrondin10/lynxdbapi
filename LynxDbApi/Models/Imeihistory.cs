﻿using System;
using System.Collections.Generic;

namespace LynxDbApi.Models
{
    public partial class IMEIHistory
    {
        public int Id { get; set; }
        public string Imei { get; set; }
        public int ScooterId { get; set; }
        public string ScooterModel { get; set; }
        public int? EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public bool IsDeleted { get; set; }
    }
}
