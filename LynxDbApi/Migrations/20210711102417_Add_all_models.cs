﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LynxDbApi.Migrations
{
    public partial class Add_all_models : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Employee",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    LastName = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    Email = table.Column<string>(maxLength: 255, nullable: true),
                    RoleId = table.Column<int>(nullable: true),
                    TerritoryId = table.Column<int>(nullable: true),
                    FullName = table.Column<string>(unicode: false, maxLength: 150, nullable: true),
                    Password = table.Column<string>(nullable: true),
                    RoleType = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    TerritoryName = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employee", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "IMEIHistory",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IMEI = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    ScooterId = table.Column<int>(nullable: false),
                    ScooterModel = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    EmployeeId = table.Column<int>(nullable: true),
                    EmployeeName = table.Column<string>(unicode: false, maxLength: 150, nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IMEIHistory", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "KeepRental",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    LastName = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    Phone = table.Column<string>(unicode: false, maxLength: 15, nullable: true),
                    Address = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    City = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    State = table.Column<string>(unicode: false, maxLength: 2, nullable: true),
                    Zipcode = table.Column<string>(unicode: false, maxLength: 12, nullable: true),
                    Email = table.Column<string>(maxLength: 255, nullable: true),
                    DriversLicense = table.Column<byte[]>(nullable: true),
                    RentalAgreement = table.Column<string>(maxLength: 255, nullable: true),
                    Insurance = table.Column<bool>(nullable: true),
                    RentalPrice = table.Column<double>(nullable: true),
                    Stripe = table.Column<bool>(nullable: true),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    DateDelivered = table.Column<DateTime>(type: "datetime", nullable: true),
                    DeliveredBy = table.Column<int>(nullable: true),
                    ChargerDelivered = table.Column<bool>(nullable: true),
                    RequestEndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    RentalEndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ChargerCollected = table.Column<bool>(nullable: true),
                    Notes = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KeepRental", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "LostScooter",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ScooterId = table.Column<int>(nullable: false),
                    TechnicianId = table.Column<int>(nullable: false),
                    ModelNumber = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    DateAdded_UTC = table.Column<DateTime>(type: "datetime", nullable: false),
                    StatusId = table.Column<int>(nullable: false),
                    StatusType = table.Column<string>(unicode: false, maxLength: 25, nullable: true),
                    Notes = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LostScooter", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Maintenance",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ServiceId = table.Column<int>(nullable: false),
                    TechnicianId = table.Column<int>(nullable: false),
                    ModelNumber = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    RepairStatus = table.Column<int>(nullable: false),
                    Notes = table.Column<string>(nullable: true),
                    RepairCost = table.Column<decimal>(type: "decimal(7, 2)", nullable: true),
                    PartsList = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    Date_Added_UTC = table.Column<DateTime>(type: "datetime", nullable: false),
                    TechName = table.Column<string>(unicode: false, maxLength: 150, nullable: true),
                    ScooterId = table.Column<int>(nullable: false),
                    Pending = table.Column<bool>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Maintenance", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "RepairStatus",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Status = table.Column<string>(unicode: false, maxLength: 25, nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RepairStatus", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "RepairTask",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Task = table.Column<string>(unicode: false, maxLength: 30, nullable: true),
                    RepairTime = table.Column<int>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RepairTask", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Scooter",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ModelNumber = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    TerritoryId = table.Column<int>(nullable: false),
                    Date_Added = table.Column<DateTime>(type: "date", nullable: true),
                    TerritoryName = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    Damaged = table.Column<bool>(nullable: true),
                    Missing = table.Column<bool>(nullable: true),
                    Irretrievable = table.Column<bool>(nullable: true),
                    Lost = table.Column<bool>(nullable: true),
                    Stolen = table.Column<bool>(nullable: true),
                    Totaled = table.Column<bool>(nullable: true),
                    LostNotes = table.Column<string>(nullable: true),
                    StolenNotes = table.Column<string>(nullable: true),
                    TotaledNotes = table.Column<string>(nullable: true),
                    IMEI = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Scooter", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "ScooterPart",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Type = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    Price = table.Column<decimal>(type: "decimal(7, 2)", nullable: true),
                    VendorId = table.Column<int>(nullable: false, defaultValueSql: "('1')"),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScooterPart", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Service",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EmployeeId = table.Column<int>(nullable: false),
                    TechnicianId = table.Column<int>(nullable: false),
                    ScooterId = table.Column<int>(nullable: false),
                    TerritoryId = table.Column<int>(nullable: true),
                    Date_Serviced_UTC = table.Column<DateTime>(type: "datetime", nullable: false),
                    StatusId = table.Column<int>(nullable: false),
                    Notes = table.Column<string>(nullable: true),
                    ScooterModel = table.Column<string>(maxLength: 50, nullable: true),
                    TerritoryName = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    StatusType = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    EmployeeName = table.Column<string>(unicode: false, maxLength: 150, nullable: true),
                    TechName = table.Column<string>(unicode: false, maxLength: 150, nullable: true),
                    Repaired = table.Column<bool>(nullable: true),
                    Found = table.Column<bool>(nullable: true),
                    Retrieved = table.Column<bool>(nullable: true),
                    Pending = table.Column<bool>(nullable: true),
                    PendingNotes = table.Column<string>(nullable: true),
                    RepairTechId = table.Column<int>(nullable: true),
                    Repair_Date = table.Column<DateTime>(type: "datetime", nullable: true),
                    FoundTechId = table.Column<int>(nullable: true),
                    Found_Date = table.Column<DateTime>(type: "datetime", nullable: true),
                    RetrieveTechId = table.Column<int>(nullable: true),
                    Retrieve_Date = table.Column<DateTime>(type: "datetime", nullable: true),
                    RepairNotes = table.Column<string>(nullable: true),
                    Pending_Date = table.Column<DateTime>(type: "datetime", nullable: true),
                    RepairTaskId = table.Column<int>(nullable: true),
                    RepairTask = table.Column<string>(unicode: false, maxLength: 30, nullable: true),
                    RepairTime = table.Column<int>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Service", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Status",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Type = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Status", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Territory",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    City = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    State = table.Column<string>(unicode: false, maxLength: 25, nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Territory", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "TotaledScooter",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ScooterId = table.Column<int>(nullable: false),
                    TechnicianId = table.Column<int>(nullable: false),
                    ModelNumber = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    DateAdded_UTC = table.Column<DateTime>(type: "datetime", nullable: false),
                    Notes = table.Column<string>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TotaledScooter", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "UserRole",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Role = table.Column<string>(unicode: false, maxLength: 25, nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRole", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Vendor",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    Phone = table.Column<string>(unicode: false, maxLength: 30, nullable: true),
                    Email = table.Column<string>(maxLength: 255, nullable: true),
                    Notes = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vendor", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Employee");

            migrationBuilder.DropTable(
                name: "IMEIHistory");

            migrationBuilder.DropTable(
                name: "KeepRental");

            migrationBuilder.DropTable(
                name: "LostScooter");

            migrationBuilder.DropTable(
                name: "Maintenance");

            migrationBuilder.DropTable(
                name: "RepairStatus");

            migrationBuilder.DropTable(
                name: "RepairTask");

            migrationBuilder.DropTable(
                name: "Scooter");

            migrationBuilder.DropTable(
                name: "ScooterPart");

            migrationBuilder.DropTable(
                name: "Service");

            migrationBuilder.DropTable(
                name: "Status");

            migrationBuilder.DropTable(
                name: "Territory");

            migrationBuilder.DropTable(
                name: "TotaledScooter");

            migrationBuilder.DropTable(
                name: "UserRole");

            migrationBuilder.DropTable(
                name: "Vendor");
        }
    }
}
