﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LynxDbApi.Models;

namespace LynxDbApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LostScootersController : ControllerBase
    {
        private readonly LynxsmDBContext _context;

        public LostScootersController(LynxsmDBContext context)
        {
            _context = context;
        }

        // GET: api/LostScooters
        [HttpGet]
        public async Task<ActionResult<IEnumerable<LostScooter>>> GetLostScooter()
        {
            return await _context.LostScooter.ToListAsync();
        }

        // GET: api/LostScooters/5
        [HttpGet("{id}")]
        public async Task<ActionResult<LostScooter>> GetLostScooter(int id)
        {
            var lostScooter = await _context.LostScooter.FindAsync(id);

            if (lostScooter == null)
            {
                return NotFound();
            }

            return lostScooter;
        }

        // PUT: api/LostScooters/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutLostScooter(int id, LostScooter lostScooter)
        {
            if (id != lostScooter.Id)
            {
                return BadRequest();
            }

            _context.Entry(lostScooter).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LostScooterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/LostScooters
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<LostScooter>> PostLostScooter(LostScooter lostScooter)
        {
            _context.LostScooter.Add(lostScooter);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetLostScooter", new { id = lostScooter.Id }, lostScooter);
        }

        // DELETE: api/LostScooters/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<LostScooter>> DeleteLostScooter(int id)
        {
            var lostScooter = await _context.LostScooter.FindAsync(id);
            if (lostScooter == null)
            {
                return NotFound();
            }

            _context.LostScooter.Remove(lostScooter);
            await _context.SaveChangesAsync();

            return lostScooter;
        }

        private bool LostScooterExists(int id)
        {
            return _context.LostScooter.Any(e => e.Id == id);
        }
    }
}
