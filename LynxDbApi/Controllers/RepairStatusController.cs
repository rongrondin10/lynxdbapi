﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LynxDbApi.Models;

namespace LynxDbApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RepairStatusController : ControllerBase
    {
        private readonly LynxsmDBContext _context;

        public RepairStatusController(LynxsmDBContext context)
        {
            _context = context;
        }

        // GET: api/RepairStatus
        [HttpGet]
        public async Task<ActionResult<IEnumerable<RepairStatus>>> GetRepairStatus()
        {
            return await _context.RepairStatus.ToListAsync();
        }

        // GET: api/RepairStatus/5
        [HttpGet("{id}")]
        public async Task<ActionResult<RepairStatus>> GetRepairStatus(int id)
        {
            var repairStatus = await _context.RepairStatus.FindAsync(id);

            if (repairStatus == null)
            {
                return NotFound();
            }

            return repairStatus;
        }

        // PUT: api/RepairStatus/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRepairStatus(int id, RepairStatus repairStatus)
        {
            if (id != repairStatus.Id)
            {
                return BadRequest();
            }

            _context.Entry(repairStatus).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RepairStatusExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/RepairStatus
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<RepairStatus>> PostRepairStatus(RepairStatus repairStatus)
        {
            _context.RepairStatus.Add(repairStatus);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetRepairStatus", new { id = repairStatus.Id }, repairStatus);
        }

        // DELETE: api/RepairStatus/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<RepairStatus>> DeleteRepairStatus(int id)
        {
            var repairStatus = await _context.RepairStatus.FindAsync(id);
            if (repairStatus == null)
            {
                return NotFound();
            }

            _context.RepairStatus.Remove(repairStatus);
            await _context.SaveChangesAsync();

            return repairStatus;
        }

        private bool RepairStatusExists(int id)
        {
            return _context.RepairStatus.Any(e => e.Id == id);
        }
    }
}
