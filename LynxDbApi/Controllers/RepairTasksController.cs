﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LynxDbApi.Models;

namespace LynxDbApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RepairTasksController : ControllerBase
    {
        private readonly LynxsmDBContext _context;

        public RepairTasksController(LynxsmDBContext context)
        {
            _context = context;
        }

        // GET: api/RepairTasks
        [HttpGet]
        public async Task<ActionResult<IEnumerable<RepairTask>>> GetRepairTask()
        {
            return await _context.RepairTask.ToListAsync();
        }

        // GET: api/RepairTasks/5
        [HttpGet("{id}")]
        public async Task<ActionResult<RepairTask>> GetRepairTask(int id)
        {
            var repairTask = await _context.RepairTask.FindAsync(id);

            if (repairTask == null)
            {
                return NotFound();
            }

            return repairTask;
        }

        // PUT: api/RepairTasks/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRepairTask(int id, RepairTask repairTask)
        {
            if (id != repairTask.Id)
            {
                return BadRequest();
            }

            _context.Entry(repairTask).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RepairTaskExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/RepairTasks
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<RepairTask>> PostRepairTask(RepairTask repairTask)
        {
            _context.RepairTask.Add(repairTask);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetRepairTask", new { id = repairTask.Id }, repairTask);
        }

        // DELETE: api/RepairTasks/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<RepairTask>> DeleteRepairTask(int id)
        {
            var repairTask = await _context.RepairTask.FindAsync(id);
            if (repairTask == null)
            {
                return NotFound();
            }

            _context.RepairTask.Remove(repairTask);
            await _context.SaveChangesAsync();

            return repairTask;
        }

        private bool RepairTaskExists(int id)
        {
            return _context.RepairTask.Any(e => e.Id == id);
        }
    }
}
