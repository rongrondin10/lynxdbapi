﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LynxDbApi.Models;

namespace LynxDbApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TerritoryController : ControllerBase
    {
        private readonly LynxsmDBContext _context;

        public TerritoryController(LynxsmDBContext context)
        {
            _context = context;
        }

        // GET: api/Territories
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Territory>>> GetTerritory()
        {
            return await _context.Territory.ToListAsync();
        }

        // GET: api/Territories/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Territory>> GetTerritory(int id)
        {
            var territory = await _context.Territory.FindAsync(id);

            if (territory == null)
            {
                return NotFound();
            }

            return territory;
        }

        // PUT: api/Territories/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTerritory(int id, Territory territory)
        {
            if (id != territory.Id)
            {
                return BadRequest();
            }

            _context.Entry(territory).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TerritoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Territories
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Territory>> PostTerritory(Territory territory)
        {
            _context.Territory.Add(territory);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTerritory", new { id = territory.Id }, territory);
        }

        // DELETE: api/Territories/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Territory>> DeleteTerritory(int id)
        {
            var territory = await _context.Territory.FindAsync(id);
            if (territory == null)
            {
                return NotFound();
            }

            _context.Territory.Remove(territory);
            await _context.SaveChangesAsync();

            return territory;
        }

        private bool TerritoryExists(int id)
        {
            return _context.Territory.Any(e => e.Id == id);
        }
    }
}
