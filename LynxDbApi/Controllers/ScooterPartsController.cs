﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LynxDbApi.Models;

namespace LynxDbApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ScooterPartsController : ControllerBase
    {
        private readonly LynxsmDBContext _context;

        public ScooterPartsController(LynxsmDBContext context)
        {
            _context = context;
        }

        // GET: api/ScooterParts
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ScooterPart>>> GetScooterPart()
        {
            return await _context.ScooterPart.ToListAsync();
        }

        // GET: api/ScooterParts/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ScooterPart>> GetScooterPart(int id)
        {
            var scooterPart = await _context.ScooterPart.FindAsync(id);

            if (scooterPart == null)
            {
                return NotFound();
            }

            return scooterPart;
        }

        // PUT: api/ScooterParts/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutScooterPart(int id, ScooterPart scooterPart)
        {
            if (id != scooterPart.Id)
            {
                return BadRequest();
            }

            _context.Entry(scooterPart).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ScooterPartExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ScooterParts
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<ScooterPart>> PostScooterPart(ScooterPart scooterPart)
        {
            _context.ScooterPart.Add(scooterPart);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetScooterPart", new { id = scooterPart.Id }, scooterPart);
        }

        // DELETE: api/ScooterParts/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ScooterPart>> DeleteScooterPart(int id)
        {
            var scooterPart = await _context.ScooterPart.FindAsync(id);
            if (scooterPart == null)
            {
                return NotFound();
            }

            _context.ScooterPart.Remove(scooterPart);
            await _context.SaveChangesAsync();

            return scooterPart;
        }

        private bool ScooterPartExists(int id)
        {
            return _context.ScooterPart.Any(e => e.Id == id);
        }
    }
}
