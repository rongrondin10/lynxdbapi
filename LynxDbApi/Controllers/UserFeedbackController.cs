﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LynxDbApi.Models;

namespace LynxDbApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserFeedbackController : ControllerBase
    {
        private readonly LynxsmDBContext _context;

        public UserFeedbackController(LynxsmDBContext context)
        {
            _context = context;
        }

        // GET: api/UserFeedback
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserFeedback>>> GetUserFeedback()
        {
            return await _context.UserFeedback.ToListAsync();
        }

        // GET: api/UserFeedback/5
        [HttpGet("{id}")]
        public async Task<ActionResult<UserFeedback>> GetUserFeedback(int id)
        {
            var userFeedback = await _context.UserFeedback.FindAsync(id);

            if (userFeedback == null)
            {
                return NotFound();
            }

            return userFeedback;
        }

        // PUT: api/UserFeedback/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUserFeedback(int id, UserFeedback userFeedback)
        {
            if (id != userFeedback.Id)
            {
                return BadRequest();
            }

            _context.Entry(userFeedback).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserFeedbackExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/UserFeedback
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<UserFeedback>> PostUserFeedback(UserFeedback userFeedback)
        {
            _context.UserFeedback.Add(userFeedback);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetUserFeedback", new { id = userFeedback.Id }, userFeedback);
        }

        // DELETE: api/UserFeedback/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<UserFeedback>> DeleteUserFeedback(int id)
        {
            var userFeedback = await _context.UserFeedback.FindAsync(id);
            if (userFeedback == null)
            {
                return NotFound();
            }

            _context.UserFeedback.Remove(userFeedback);
            await _context.SaveChangesAsync();

            return userFeedback;
        }

        private bool UserFeedbackExists(int id)
        {
            return _context.UserFeedback.Any(e => e.Id == id);
        }
    }
}
