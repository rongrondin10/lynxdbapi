﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LynxDbApi.Models;

namespace LynxDbApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class KeepRentalController : ControllerBase
    {
        private readonly LynxsmDBContext _context;

        public KeepRentalController(LynxsmDBContext context)
        {
            _context = context;
        }

        // GET: api/KeepRental
        [HttpGet]
        public async Task<ActionResult<IEnumerable<KeepRental>>> GetKeepRental()
        {
            return await _context.KeepRental.ToListAsync();
        }

        // GET: api/KeepRental/5
        [HttpGet("{id}")]
        public async Task<ActionResult<KeepRental>> GetKeepRental(int id)
        {
            var keepRental = await _context.KeepRental.FindAsync(id);

            if (keepRental == null)
            {
                return NotFound();
            }

            return keepRental;
        }

        // PUT: api/KeepRental/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutKeepRental(int id, KeepRental keepRental)
        {
            if (id != keepRental.Id)
            {
                return BadRequest();
            }

            _context.Entry(keepRental).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!KeepRentalExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/KeepRental
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<KeepRental>> PostKeepRental(KeepRental keepRental)
        {
            _context.KeepRental.Add(keepRental);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetKeepRental", new { id = keepRental.Id }, keepRental);
        }

        // DELETE: api/KeepRental/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<KeepRental>> DeleteKeepRental(int id)
        {
            var keepRental = await _context.KeepRental.FindAsync(id);
            if (keepRental == null)
            {
                return NotFound();
            }

            _context.KeepRental.Remove(keepRental);
            await _context.SaveChangesAsync();

            return keepRental;
        }

        private bool KeepRentalExists(int id)
        {
            return _context.KeepRental.Any(e => e.Id == id);
        }
    }
}
