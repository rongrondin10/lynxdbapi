﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LynxDbApi.Models;

namespace LynxDbApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ScooterController : ControllerBase
    {
        private readonly LynxsmDBContext _context;

        public ScooterController(LynxsmDBContext context)
        {
            _context = context;
        }

        // GET: api/Scooters
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Scooter>>> GetScooter()
        {
            return await _context.Scooter.ToListAsync();
        }

        // GET: api/Scooters/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Scooter>> GetScooter(int id)
        {
            var scooter = await _context.Scooter.FindAsync(id);

            if (scooter == null)
            {
                return NotFound();
            }

            return scooter;
        }

        // PUT: api/Scooters/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutScooter(int id, Scooter scooter)
        {
            if (id != scooter.Id)
            {
                return BadRequest();
            }

            _context.Entry(scooter).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ScooterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Scooters
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Scooter>> PostScooter(Scooter scooter)
        {
            _context.Scooter.Add(scooter);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetScooter", new { id = scooter.Id }, scooter);
        }

        // DELETE: api/Scooters/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Scooter>> DeleteScooter(int id)
        {
            var scooter = await _context.Scooter.FindAsync(id);
            if (scooter == null)
            {
                return NotFound();
            }

            _context.Scooter.Remove(scooter);
            await _context.SaveChangesAsync();

            return scooter;
        }

        private bool ScooterExists(int id)
        {
            return _context.Scooter.Any(e => e.Id == id);
        }
    }
}
