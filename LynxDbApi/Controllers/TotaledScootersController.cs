﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LynxDbApi.Models;

namespace LynxDbApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TotaledScootersController : ControllerBase
    {
        private readonly LynxsmDBContext _context;

        public TotaledScootersController(LynxsmDBContext context)
        {
            _context = context;
        }

        // GET: api/TotaledScooters
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TotaledScooter>>> GetTotaledScooter()
        {
            return await _context.TotaledScooter.ToListAsync();
        }

        // GET: api/TotaledScooters/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TotaledScooter>> GetTotaledScooter(int id)
        {
            var totaledScooter = await _context.TotaledScooter.FindAsync(id);

            if (totaledScooter == null)
            {
                return NotFound();
            }

            return totaledScooter;
        }

        // PUT: api/TotaledScooters/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTotaledScooter(int id, TotaledScooter totaledScooter)
        {
            if (id != totaledScooter.Id)
            {
                return BadRequest();
            }

            _context.Entry(totaledScooter).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TotaledScooterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/TotaledScooters
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<TotaledScooter>> PostTotaledScooter(TotaledScooter totaledScooter)
        {
            _context.TotaledScooter.Add(totaledScooter);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTotaledScooter", new { id = totaledScooter.Id }, totaledScooter);
        }

        // DELETE: api/TotaledScooters/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<TotaledScooter>> DeleteTotaledScooter(int id)
        {
            var totaledScooter = await _context.TotaledScooter.FindAsync(id);
            if (totaledScooter == null)
            {
                return NotFound();
            }

            _context.TotaledScooter.Remove(totaledScooter);
            await _context.SaveChangesAsync();

            return totaledScooter;
        }

        private bool TotaledScooterExists(int id)
        {
            return _context.TotaledScooter.Any(e => e.Id == id);
        }
    }
}
