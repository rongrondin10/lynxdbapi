﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LynxDbApi.Models;

namespace LynxDbApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IMEIHistoryController : ControllerBase
    {
        private readonly LynxsmDBContext _context;

        public IMEIHistoryController(LynxsmDBContext context)
        {
            _context = context;
        }

        // GET: api/IMEIHistory
        [HttpGet]
        public async Task<ActionResult<IEnumerable<IMEIHistory>>> GetIMEIHistory()
        {
            return await _context.IMEIHistory.ToListAsync();
        }

        // GET: api/IMEIHistory/5
        [HttpGet("{id}")]
        public async Task<ActionResult<IMEIHistory>> GetIMEIHistory(int id)
        {
            var IMEIHistory = await _context.IMEIHistory.FindAsync(id);

            if (IMEIHistory == null)
            {
                return NotFound();
            }

            return IMEIHistory;
        }

        // PUT: api/IMEIHistory/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutIMEIHistory(int id, IMEIHistory IMEIHistory)
        {
            if (id != IMEIHistory.Id)
            {
                return BadRequest();
            }

            _context.Entry(IMEIHistory).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!IMEIHistoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/IMEIHistory
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<IMEIHistory>> PostIMEIHistory(IMEIHistory IMEIHistory)
        {
            _context.IMEIHistory.Add(IMEIHistory);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetIMEIHistory", new { id = IMEIHistory.Id }, IMEIHistory);
        }

        // DELETE: api/IMEIHistory/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<IMEIHistory>> DeleteIMEIHistory(int id)
        {
            var IMEIHistory = await _context.IMEIHistory.FindAsync(id);
            if (IMEIHistory == null)
            {
                return NotFound();
            }

            _context.IMEIHistory.Remove(IMEIHistory);
            await _context.SaveChangesAsync();

            return IMEIHistory;
        }

        private bool IMEIHistoryExists(int id)
        {
            return _context.IMEIHistory.Any(e => e.Id == id);
        }
    }
}
